package main

import (
	"fmt"
	"strconv"
	"strings"
)

func Calculate(str string) string {
	problems := strings.Split(str, " ")

	if len(problems)%2 == 0 {
		return "Invalid Input"
	}

	var res int
	var operator = "+"
	for i := 0; i < len(problems); i++ {
		if i%2 == 0 {
			num, err := strconv.Atoi(problems[i])
			if err != nil {
				return "Invalid Input"
			}

			if operator == "+" {
				res += num
			} else if operator == "-" {
				res -= num
			} else {
				return "Invalid Input"
			}
		} else {
			operator = problems[i]
		}
	}

	return strconv.Itoa(res)
}

func main() {
	str := "5000 + 3 - 10000 + 1500 - 01"

	fmt.Println(Calculate(str))
}
