package main

import (
	"strconv"
	"testing"
)

func TestCalculateWithTrueInput(t *testing.T) {
	input := "5000 + 3 - 10000 + 1500 - 01"
	expectedOutput := -3498
	actual, err := strconv.Atoi(Calculate(input))

	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}

	if actual != expectedOutput {
		t.Errorf("Expected output to be %v, but got %v", expectedOutput, actual)
	}
}

func TestCalculateWithWrongInput1(t *testing.T) {
	input := "5000 + 3 - 10000 + 1500 - 01 + "
	expectedOutput := "Invalid Input"
	actual := Calculate(input)

	if actual != expectedOutput {
		t.Errorf("Expected output to be %v, but got %v", expectedOutput, actual)
	}
}

func TestCalculateWithWrongInput2(t *testing.T) {
	input := "5000 + + 10000 + 1500 - 01"
	expectedOutput := "Invalid Input"
	actual := Calculate(input)

	if actual != expectedOutput {
		t.Errorf("Expected output to be %v, but got %v", expectedOutput, actual)
	}
}
